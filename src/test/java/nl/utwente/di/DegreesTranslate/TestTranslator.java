package nl.utwente.di.DegreesTranslate;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TestTranslator {

    @Test
    public void testBook1 () throws Exception {
        Translator translator = new Translator();
        double price = translator.getFahrenheitDegrees("0");
        Assertions.assertEquals(32, price, 0.0, "Degrees");
    }
}
