package nl.utwente.di.DegreesTranslate;

public class Translator {
    public double getFahrenheitDegrees(String cDegrees) {

        return Double.parseDouble(cDegrees) + 32.0;
    }
}
