package nl.utwente.di.DegreesTranslate;

import java.io.*;

import jakarta.servlet.*;
import jakarta.servlet.http.*;

/**
 * Example of a Servlet that gets degrees in Celsius and returns Fahrenheit degrees
 */

public class DegreesTranslate extends HttpServlet {
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private Translator translator;

    public void init() throws ServletException {
        translator = new Translator();
    }

    public void doGet(HttpServletRequest request,
                      HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        String title = "Temperature Convertor";

        // Done with string concatenation only for the demo
        // Not expected to be done like this in the project
        out.println("<!DOCTYPE HTML>\n" +
                "<HTML>\n" +
                "<HEAD><TITLE>" + title + "</TITLE>" +
                "<LINK REL=STYLESHEET HREF=\"styles.css\">" +
                "</HEAD>\n" +
                "<BODY BGCOLOR=\"#FDF5E6\">\n" +
                "<H1>" + title + "</H1>\n" +
                "  <P>Degrees in Celsius:" +
                request.getParameter("cDegrees") + "\n" +
                "  Fahrenheit Degrees: " +
                translator.getFahrenheitDegrees(request.getParameter("cDegrees")) +
                "</BODY></HTML>");
    }
}
